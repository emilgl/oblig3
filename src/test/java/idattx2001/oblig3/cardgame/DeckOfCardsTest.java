package idattx2001.oblig3.cardgame;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeckOfCardsTest {
    @Test
    public void testIfRightAmountOfCardsAreCreated(){
        DeckOfCards deckOfCards = new DeckOfCards();
        assertEquals(deckOfCards.getPlayingCards().size(),52);
    }
    @Test
    public void testIfThereAreDifferentCardsCreated(){
        DeckOfCards deckOfCards = new DeckOfCards();
        assertEquals(deckOfCards.getPlayingCards().get(51).getAsString(),"C13");
        assertEquals(deckOfCards.getPlayingCards().get(5).getAsString(), "S6");
    }
    @Test
    public void testIfThereAreDealtAHandWithRightAmountOfCards(){
        DeckOfCards deckOfCards = new DeckOfCards();
        assertEquals(deckOfCards.dealHand(5).size(), 5);
    }


}
