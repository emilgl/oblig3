package idattx2001.oblig3.cardgame;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
public class HandOfCardsTest {
    @Test
    public void checkHandIsMade(){
        HandOfCards handOfCards = new HandOfCards(new DeckOfCards().dealHand(5));
        assertEquals(handOfCards.getHand(5).size(),5 );
    }
    @Test
    public void checkSum(){
        ArrayList<PlayingCard> args = new ArrayList<>();
        HandOfCards handOfCards = new HandOfCards(args);
        args.add(new PlayingCard('♠', 1));
        args.add(new PlayingCard('♠', 2));
        args.add(new PlayingCard('♠', 3));
        assertEquals(handOfCards.checkSum(),6 );
    }
    @Test
    public void checkHearts(){
        ArrayList<PlayingCard> args = new ArrayList<>();
        HandOfCards handOfCards = new HandOfCards(args);
        args.add(new PlayingCard('♠', 1));
        args.add(new PlayingCard('♠', 2));
        args.add(new PlayingCard('♠', 3));
        args.add(new PlayingCard('♥', 4));
        args.add(new PlayingCard('♥', 5));
        assertEquals(handOfCards.checkHearts().size(),2);
    }
    @Test
    public void checkQueenOfSpades() {
        ArrayList<PlayingCard> args = new ArrayList<>();
        HandOfCards handOfCards = new HandOfCards(args);
        args.add(new PlayingCard('♠', 1));
        args.add(new PlayingCard('♠', 2));
        args.add(new PlayingCard('♠', 12));
        args.add(new PlayingCard('♥', 4));
        args.add(new PlayingCard('♥', 5));
        assertTrue(handOfCards.checkQueenOfSpades());
    }
    @Test
    public void checkFlush() {
        ArrayList<PlayingCard> args = new ArrayList<>();
        HandOfCards handOfCards = new HandOfCards(args);
        args.add(new PlayingCard('♠', 1));
        args.add(new PlayingCard('♠', 2));
        args.add(new PlayingCard('♠', 12));
        args.add(new PlayingCard('♠', 4));
        args.add(new PlayingCard('♠', 5));
        assertTrue(handOfCards.checkFlush());
    }

}
