package idattx2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Representing a hand of cards and contains different methods to check a hand for a flush,
 * queen of spades, hearts and the sum of faces.
 */
public class HandOfCards {
    private List<PlayingCard> hand = Collections.emptyList();
    private final DeckOfCards deckOfCards = new DeckOfCards();

    /**
     * Creates an instance of a Hand
     */
    public HandOfCards(){
    }

    /**
     * Creates an instance of a hand used in testing in the HandOfCardsTest class
     * @param hand takes an ArrayList as parameter
     */
    public HandOfCards(ArrayList<PlayingCard> hand) {
        this.hand = hand;
    }

    /**
     * Returning a hand
     * @param n takes an int which decides how many cards that will be dealt with the dealHand
     *          method
     * @return returns the hand with a List
     */
    public List<PlayingCard> getHand(int n) {
        hand = deckOfCards.dealHand(n);
        return hand;
    }

    /**
     * Method to check sum
     * @return returns an int with the sum
     */
    public int checkSum(){
        return hand.stream().map(PlayingCard::getFace).reduce(0, Integer::sum);
    }

    /**
     * Method to check if there are hearts in the hand
     * @return returns a list with all heart cards
     */
    public List<PlayingCard> checkHearts(){
        return hand.stream().filter(p -> p.getSuit() == '♥').collect(Collectors.toList());
    }

    /**
     * Method to check if there is a queen of spades
     * @return return true if the card is in the hand and false if it is not
     */
    public boolean checkQueenOfSpades(){
        return hand.stream().anyMatch(p -> p.getAsString().equals("♠12"));
    }

    /**
     * Method to check for a flush
     * @return true if there is a flush and false if it is not
     */
    public boolean checkFlush(){
        return (hand.stream().filter(p -> p.getSuit() == '♠').count() > 4) ||
                (hand.stream().filter(p -> p.getSuit() == '♥').count() > 4) ||
                (hand.stream().filter(p -> p.getSuit() == '♦').count() > 4) ||
                (hand.stream().filter(p -> p.getSuit() == '♣').count() > 4);
    }

    /**
     * Method to display the hand
     * @param n takes an int as parameter which is used to ultimately choose how many cards that
     *          will be dealt
     * @return returns a String
     */
    public String showHand(int n){
        StringBuilder sb = new StringBuilder();
        for (PlayingCard playingCard:getHand(n)){
            sb.append(playingCard.getAsString()).append(" ");
        }
        return sb.toString();
    }

}
