package idattx2001.oblig3.cardgame;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.application.Application;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application implements EventHandler<ActionEvent> {
    private Button dealHand;
    private Button checkHand;
    private Text displayHand;
    private final Text CHECK_SUM_OF_FACES = new Text("Sum of Faces: ");
    private final Text CHECK_CARDS_OF_HEARTS = new Text("Cards of Hearts: ");
    private final Text CHECK_QUEEN_OF_SPADES = new Text("Queen of Spades: ");
    private final Text CHECK_FLUSH = new Text("Flush: ");
    private TextField checkSumOfFaces = new TextField("Check Hand");
    private TextField checkCardsOfHearts = new TextField("Check Hand");
    private TextField checkQueenOfSpades = new TextField("Check Hand");
    private TextField checkFlush =new TextField("Check Hand") ;
    private HandOfCards handOfCards = new HandOfCards();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Card Game");

        dealHand = new Button("Deal Hand");
        dealHand.setOnAction(this);
        checkHand = new Button("Check Hand");
        checkHand.setOnAction(this);

        displayHand = new Text("Deal a Hand of Cards");
        displayHand.setScaleX(3);
        displayHand.setScaleY(3);

        Group group = new Group();
        group.getChildren().add(displayHand);

        BorderPane borderPane = new BorderPane();
        borderPane.setRight(addVBoxButton());
        borderPane.setCenter(group);
        borderPane.setBottom(addHBox());

        borderPane.setPadding(new Insets(0, 0, 100, 30));

        Scene scene = new Scene(borderPane, 750, 650);
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        if(actionEvent.getSource()==dealHand){
            displayHand.setText(handOfCards.showHand(5));
        }

        if(actionEvent.getSource()==checkHand){
            checkSumOfFaces.setText(""+ handOfCards.checkSum());
            if(handOfCards.checkHearts().isEmpty()){
                checkCardsOfHearts.setText("No Hearts");
            }else {
                StringBuilder sb = new StringBuilder();
                for (PlayingCard playingCard:handOfCards.checkHearts()) {
                    sb.append(playingCard.getAsString()).append(" ");

                }
                checkCardsOfHearts.setText(sb.toString());
            }
            if(handOfCards.checkQueenOfSpades()){
                checkQueenOfSpades.setText("YES");
            }else {
                checkQueenOfSpades.setText("Not this time");
            }
            if(handOfCards.checkFlush()){
                checkFlush.setText("Flush\uD83C\uDF89\uD83C\uDF89");
            }else{
                checkFlush.setText("Try again");
            }
        }
    }

    public VBox addVBoxButton() {
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(130, 50, 0, 0));
        vbox.setSpacing(40);

        Button[] options = new Button[]{
                dealHand, checkHand
        };

        for (int i=0; i<2; i++) {
            vbox.getChildren().add(options[i]);
        }
        return vbox;
    }

    public VBox addVBoxText() {
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(30));
        vbox.setSpacing(20);

        Text[] options = new Text[]{
                CHECK_SUM_OF_FACES, CHECK_CARDS_OF_HEARTS, CHECK_QUEEN_OF_SPADES, CHECK_FLUSH
        };

        for (int i = 0; i < 4; i++) {
            vbox.getChildren().add(options[i]);
        }
        return vbox;
    }

    public VBox addVBoxTextFields() {
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(30));
        vbox.setSpacing(10);

        TextField[] options = new TextField[]{
                checkSumOfFaces, checkCardsOfHearts, checkQueenOfSpades, checkFlush
        };

        for (int i = 0; i < 4; i++) {
            vbox.getChildren().add(options[i]);
        }
        return vbox;
    }

    public HBox addHBox(){
        HBox hBox = new HBox();
        hBox.getChildren().add(addVBoxText());
        hBox.getChildren().add(addVBoxTextFields());
        return hBox;
    }


    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }
}

