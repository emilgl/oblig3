package idattx2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Random;

/**
 * Representing a complete deck of cards
 */
public class DeckOfCards {
    private final char[] suit = { '♠', '♥', '♦', '♣'};
    private final char[] face = {1,2,3,4,5,6,7,8,9,10,11,12,13};
    private final ArrayList<PlayingCard> playingCards;

    /**
     * Creates an instance of a deck of cards by going trough the suit list one suit at a time
     * and for every suit its going trough the face list to create every PlayingCard in a deck
     */
    public DeckOfCards(){
        this.playingCards = new ArrayList<>();
        for (char s : suit) {
            for (char f : face) {
                playingCards.add(new PlayingCard(s, f));
            }
        }
    }

    public ArrayList<PlayingCard> getPlayingCards() {
        return playingCards;
    }

    public char[] getFace() {
        return face;
    }

    public char[] getSuit() {
        return suit;
    }

    /**
     * Method to deal a new random hand if the same card is randomly selected twice it will
     * not be added and it will be selected a new one
     * @param n takes a int which decides how many cards that will be dealt
     * @return returns an arraylist with the a hand of cards
     */
    public ArrayList<PlayingCard> dealHand(int n){
        ArrayList<PlayingCard> handOfCards = new ArrayList<>();
        Random random = new Random();
        for (int i =0; i < n;i++){
            int randomNumber = random.nextInt(52);
            if(handOfCards.contains(playingCards.get(randomNumber))){
                i--;
            }else {
                handOfCards.add(playingCards.get(randomNumber));
            }
        }
        return handOfCards;
    }

}
